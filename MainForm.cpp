#include "MainForm.h"
#include "ui_MainForm.h"

#include <QFileDialog>

MainForm::MainForm(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainForm)
  , imgView_(new QtImageViewer)
{
    ui->setupUi(this);

    this->setCentralWidget(imgView_);

    connect(ui->actionOpen, &QAction::triggered, this, &MainForm::onOpenFile);
}

MainForm::~MainForm()
{

}

void MainForm::onOpenFile()
{
    QString fileName = QFileDialog::getOpenFileName(
                this,
                "Open file",
                "",
                QString("Images (*.png *.bmp *.jpg *.jpeg)"));
    if(fileName.isEmpty())return;

    imgView_->onSetImage(QImage(fileName));
}


