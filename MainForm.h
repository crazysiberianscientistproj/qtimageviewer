#ifndef MAINFORM_H
#define MAINFORM_H

#include <QMainWindow>

#include "QtImageViewer.h"

namespace Ui {
class MainForm;
}

class MainForm : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainForm(QWidget *parent = 0);
    ~MainForm();


private:

public slots:
    void onOpenFile();

private:
    Ui::MainForm *ui;

    QtImageViewer *imgView_;
};

#endif // MAINFORM_H
