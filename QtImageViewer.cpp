#include "QtImageViewer.h"

#include <QtWidgets>
#include <QLayout>

#include "ViewerControl.h"

QtImageViewer::QtImageViewer(QWidget *parent):
   QWidget(parent)
   , imageLabel_(new QLabel)
   , scrollArea_(new QScrollArea)
   , mainLayout_(new QGridLayout(this))
   , scaleFactor_(1)
   , viewControl_(new ViewerControl)
{
    this->initForm();
    this->initConnections();
}

void QtImageViewer::onSetImage(const QImage &newImage)
{
    image_ = newImage;
    imageLabel_->setVisible(true);
    imageLabel_->setPixmap(QPixmap::fromImage(image_));
    scaleFactor_ = 1.0;

    imageLabel_->adjustSize();
}

void QtImageViewer::onScaleImage(double factor)
{
    if(!imageLabel_->isVisible()) return;

    imageLabel_->resize(factor * imageLabel_->pixmap()->size());
}

void QtImageViewer::initForm()
{
    this->addImageView(0,0);
}

void QtImageViewer::initConnections()
{
    connect(viewControl_, SIGNAL(scaleFactor(double)),
            this, SLOT(onScaleImage(double)));
}

void QtImageViewer::addImageView(int row, int col)
{
    imageLabel_->setBackgroundRole(QPalette::Base);
    imageLabel_->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    imageLabel_->setScaledContents(true);

    scrollArea_->setBackgroundRole(QPalette::Dark);
    scrollArea_->setWidget(imageLabel_);
    imageLabel_->setVisible(false);

    mainLayout_->addWidget(viewControl_, row, col);
    mainLayout_->addWidget(scrollArea_, row+1, col);
}
