#ifndef QTIMAGEVIEWER_H
#define QTIMAGEVIEWER_H

#include <QMainWindow>
#include <QImage>

QT_BEGIN_NAMESPACE
class QAction;
class QLabel;
class QMenu;
class QScrollArea;
class QScrollBar;
class QGridLayout;
QT_END_NAMESPACE

class ViewerControl;

class QtImageViewer : public QWidget
{
    Q_OBJECT

public:
    QtImageViewer(QWidget *parent = 0);

public slots:
    virtual void onSetImage(const QImage &newImage);
    virtual void onScaleImage(double factor);
//    virtual void onZoomIn();
//    virtual void onZoomOut();
//    virtual void onNormalSize();

protected:
    virtual void initForm();
    virtual void initConnections();
    virtual void addImageView(int row, int col);
//    virtual void adjustScrollBar(QScrollBar *scrollBar, double factor);

    QGridLayout *mainLayout_;
    QImage image_;
    QLabel *imageLabel_;
    QScrollArea *scrollArea_;
    ViewerControl *viewControl_;

    double scaleFactor_;
};

#endif
