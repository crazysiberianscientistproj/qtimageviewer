#QT += widgets

INCLUDEPATH += \
    $$PWD

HEADERS       += \
    $$PWD/QtImageViewer.h \
    $$PWD/ViewerControl.h

SOURCES       += \
    $$PWD/QtImageViewer.cpp \
    $$PWD/ViewerControl.cpp

RESOURCES += \
    $$PWD/icons.qrc
