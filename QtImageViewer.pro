QT += widgets

HEADERS       = \
    QtImageViewer.h \
    MainForm.h \
    ViewerControl.h

SOURCES       = \
    main.cpp \
    QtImageViewer.cpp \
    MainForm.cpp \
    ViewerControl.cpp

FORMS += \
    MainForm.ui

RESOURCES += \
    icons.qrc
