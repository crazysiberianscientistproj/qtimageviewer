#include "ViewerControl.h"

#include <QDebug>
#include <QDoubleSpinBox>
#include <QLabel>
#include <QPixmap>

ViewerControl::ViewerControl(QWidget *parent):
    QToolBar(parent)
{
    this->initForm();
}

void ViewerControl::initForm()
{
    // Scale box
    QLabel *icon (new QLabel);
    icon->setPixmap(QPixmap("://resources/magnifier-zoom.png"));
    icon->adjustSize();
    this->addWidget(icon);
    this->addWidget(new QLabel(":"));

    QDoubleSpinBox *scaleBox(new QDoubleSpinBox);
    scaleBox->setDecimals(5);
    scaleBox->setValue(1);
    this->addWidget(scaleBox);
    connect(scaleBox, SIGNAL(valueChanged(double)), this, SLOT(onScaleFactor(double)));

    this->addSeparator();

    // ~Scale box
}

void ViewerControl::onScaleFactor(double factor)
{
    emit this->scaleFactor(factor);
}
