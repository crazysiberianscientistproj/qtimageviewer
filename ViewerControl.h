#ifndef VIEWERCONTROL_H
#define VIEWERCONTROL_H

#include <QToolBar>
#include <QObject>

class ViewerControl : public QToolBar
{
    Q_OBJECT

public:
    ViewerControl(QWidget *parent = 0);

private:
    void initForm();

signals:
    void scaleFactor(double factor);

private slots:
    void onScaleFactor(double factor);
};

#endif // VIEWERCONTROL_H
